import java.util.concurrent.ThreadLocalRandom;

public class DemoThreadPrice {

    static class PriceShare {
        public static double generateNewPrice(int price) {
            double x = ThreadLocalRandom.current().nextDouble(-0.03, 0.03);
            return (price * x) + price;
        }
    }
}
