import java.util.Date;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DemoBroker {


    static class Broker {
        static Date date = new Date();
        static Lock lock = new ReentrantLock();
        static Condition condition = lock.newCondition();

        public static void broker(String name, int value1, int value2, int value3) throws InterruptedException {
            lock.lock();
            while (value1 > DemoTrading.simulator.balanceFirstCompany && value2 > DemoTrading.simulator.balanceSecondCompany
                    && value3 > DemoTrading.simulator.balanceThirdCompany) {
                condition.await();
            }
            System.out.println(date + "Balance before buying: " + DemoTrading.simulator.balanceFirstCompany + " "
                    + DemoTrading.simulator.balanceSecondCompany + " " + DemoTrading.simulator.balanceThirdCompany);
            DemoTrading.simulator.balanceFirstCompany -= value1;
            DemoTrading.simulator.balanceSecondCompany -= value2;
            DemoTrading.simulator.balanceThirdCompany -= value3;
            System.out.println("Broker: " + name + " balance after buying: " + DemoTrading.simulator.balanceFirstCompany + " "
                    + DemoTrading.simulator.balanceSecondCompany + " " + DemoTrading.simulator.balanceThirdCompany);
            lock.unlock();
        }
    }
}
