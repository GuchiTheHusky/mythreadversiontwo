public class Run {


    static class MyDeposit extends Thread {
        @Override
        public void run() {
            try {
                for (int i = 0; i < 5; i++) {
                    DemoTrading.simulator.deposit(450, 750, 500);
                    Thread.yield();
                    Thread.sleep(3000);
                    System.out.println();
                }
            } catch (InterruptedException e) {
                System.out.println();
            }
        }
    }
    static class WithDrawAlice extends Thread {
        @Override
        public void run() {
            try {
                for (int i = 0; i < 15; i++) {
                    Thread.sleep(1000);
                    DemoBroker.Broker.broker("\"ALice\"", 9, 18, 12);
                    System.out.println();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
    static class WithDrawBob extends Thread {
        @Override
        public void run() {
            try {
                for (int i = 0; i < 15; i++) {
                    Thread.sleep(1000);
                    DemoBroker.Broker.broker("\"Bob\"", 14, 15, 17);
                    System.out.println();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
    static class WithDrawCharlie extends Thread {
        @Override
        public void run() {
            try {
                for (int i = 0; i < 15; i++) {
                    Thread.sleep(1000);
                   DemoBroker.Broker.broker("\"Charlie\"", 11, 13, 16);
                    System.out.println();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}




