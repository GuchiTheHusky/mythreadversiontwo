import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("<<<Trading start>>>");
        ExecutorService service = Executors.newCachedThreadPool();

        service.execute(new Run.MyDeposit());
        service.execute(new Run.WithDrawAlice());
        service.execute(new Run.WithDrawBob());
        service.execute(new Run.WithDrawCharlie());

        service.shutdown();

        Thread.sleep(25000);
        System.out.println("<<<Trading End>>>");
    }
}