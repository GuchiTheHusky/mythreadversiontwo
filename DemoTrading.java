import java.util.Date;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DemoTrading {

    static DemoTradingSimulator simulator = new DemoTradingSimulator();

    static class DemoTradingSimulator {

        Date date = new Date();
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        String firstCompany = "Xiaomi";
        String secondCompany = "Apple";
        String thirdCompany = "Samsung";
        int balanceFirstCompany = 0;
        int balanceSecondCompany = 0;
        int balanceThirdCompany = 0;

        public void deposit(int value1, int value2, int value3) throws InterruptedException {
            lock.lock();
            Thread.sleep(100);
            double priceFirstShare = DemoThreadPrice.PriceShare.generateNewPrice(151);
            double priceSecondShare = DemoThreadPrice.PriceShare.generateNewPrice(321);
            double priceThirdShare = DemoThreadPrice.PriceShare.generateNewPrice(147);
            String finalPriceFirst = String.format("%.2f", priceFirstShare);
            String finalPriceSecond = String.format("%.2f", priceSecondShare);
            String finalPriceThird = String.format("%.2f", priceThirdShare);
            balanceFirstCompany = value1;
            balanceSecondCompany = value2;
            balanceThirdCompany = value3;
            System.out.println(date + " Company: [" + firstCompany + " amount: " + balanceFirstCompany +
                    " price: " + finalPriceFirst + "]  [" + secondCompany + " amount: " + balanceSecondCompany +
                    " price: " + finalPriceSecond + "]  ["
                    + thirdCompany + " amount: " + balanceThirdCompany + " price: " + finalPriceThird + "]");
            condition.signalAll();
            lock.unlock();
        }
    }
}
